


import React, { useEffect, useState } from 'react';
import { View, Text, ActivityIndicator, Image, TouchableOpacity } from 'react-native';
import { globalStyles } from '../../Styles';
import { useNavigation } from '@react-navigation/native';
import FontsFam from './FontsFam';
import { literals } from '../Literal';

const Notification = () => {
  const [profileData, setProfileData] = useState(new Array(6).fill(null));
  const [loading, setLoading] = useState(new Array(6).fill(true));
  const [error, setError] = useState(new Array(6).fill(false));
  const navigation = useNavigation();

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('https://03ba0f69-d132-4388-90a6-670fb8e815cd.mock.pstmn.io/foodData');
      if (!response.ok) throw new Error('Failed to fetch data');
      const jsonData = await response.json();

      // Assuming jsonData is an array of 3 items
      const repeatedData = [];
      for (let i = 0; i < 6; i++) {
        repeatedData.push(jsonData[i % 3]); // Repeat the data
      }

      setProfileData(repeatedData);
      setLoading(new Array(6).fill(false));
    } catch (error) {
      console.error('Error fetching data:', error);
      setError(new Array(6).fill(true));
      setLoading(new Array(6).fill(false));
    }
  };

  const handleProfilePress = (index) => {
    const data = profileData[index];
    if (data && data.profile) {
      navigation.navigate('ProfileDetails', { profileData: data });
    } else {
      console.error(`Invalid profile data for index ${index}`);
      // Handle the error appropriately, e.g., show a message to the user
    }
  };

  return (
    <FontsFam>

      <View style={globalStyles.containerNotification1}>
        {profileData.map((data, index) => (
          <View key={index}>
            {loading[index] ? (
              <ActivityIndicator size="small" color="#0000ff" />

            ) : error[index] ? (
              <Text>Error loading profile {index + 1}</Text>
            ) : data && data.profile ? (

              <View>
                <View style={globalStyles.cardIdOneMain}>
                  {index < profileData.length - 1 && (
                    <Text style={globalStyles.additionalText}>Today</Text>
                  )}
                </View>
                <View style={globalStyles.cardIdOne}>
                  <TouchableOpacity style={globalStyles.cardIdOne1} onPress={() => handleProfilePress(index)}>
                    <View style={globalStyles.imageMain}>
                      <Image source={{ uri: data.profile }} style={globalStyles.imageNotification} />
                    </View>
                    <View style={globalStyles.notificationProfile}>
                      <Text style={globalStyles.textone}>{data.profileName}</Text>
                      <Text style={globalStyles.texttwo}>{literals.followNotification}</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity style={globalStyles.followButton} onPress={() => console.log(`Follow button pressed for index ${index}`)}>
                    <Text style={globalStyles.followButtoon}>{literals.btnNotification}</Text>
                  </TouchableOpacity>
                </View>

              </View>
            ) : (
              <Text>{literals[`id${index + 1}Notification`]}</Text>
            )}

          </View>
        ))}
      </View>
    </FontsFam>
  );
};

export default Notification;













